import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';


@Injectable()
export class VariableShareService {
  public globalVar = 'Oll  value';
  public scrHeight = '100px';
  public scrWidth = '100px';
  public closeSidebar: boolean = false;
  public theme: string = 'light';
  private sharedDataSubject = new BehaviorSubject<boolean>(false);
  sharedData$ = this.sharedDataSubject.asObservable();

  private sharedDataThemeSubject = new BehaviorSubject<any>('light');
  sharedDataTheme$ = this.sharedDataThemeSubject.asObservable();


  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  setCloseSidebar(newValue: boolean) {
    console.log(newValue);
    this.sharedDataSubject.next(newValue);
  }

  getCloseSidebar(): boolean {
    console.log(this.closeSidebar);
    return this.closeSidebar;
  }


  setTheme(newValue: any) {
    this.sharedDataThemeSubject.next(newValue);
  }
  getTheme():any {
    return this.theme;
  }
  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }
}